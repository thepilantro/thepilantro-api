import { Controller, Get } from "@nestjs/common";
import { FacebookService } from "./facebook.service";

@Controller("facebook")
export class FacebookController {
  constructor(private readonly facebookService: FacebookService) {}

  // get facebook access token
  @Get("access_token")
  fbAccessToken() {
    return this.facebookService.getAccessToken();
  }
}
