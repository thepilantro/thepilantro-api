import { Injectable, HttpService } from "@nestjs/common";
import configuration from "src/common/config/configuration";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class FacebookService {
  constructor(private readonly httpService: HttpService) {}

  // get facebook access token
  getAccessToken(): Observable<any> {
    return this.httpService
      .get(
        `https://graph.facebook.com/oauth/access_token?client_id=${
          configuration().facebook.id
        }&client_secret=${configuration().facebook.secret}&grant_type=${
          configuration().facebook.grantType
        }`
      )
      .pipe(
        map(res => {
          return res.data;
        })
      );
  }
}
