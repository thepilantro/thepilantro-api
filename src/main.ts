import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { AllExceptionsFilter } from "./common/filters/all-exceptions.filter";
import { logger } from "./common/middlewares/logger.middleware";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);

  app.use(logger);
  app.useGlobalFilters(new AllExceptionsFilter());
}
bootstrap();
