import { ObjectID } from "mongodb";

export class DonationDto {
  _id: ObjectID;
  userId: ObjectID;
  ngoId: ObjectID;
  amount: number;
  comment: string;
  isAnonymous: boolean;
}
