import { Module } from "@nestjs/common";
import { DonationsService } from "./donations.service";
import { DonationsController } from "./donations.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { DonationSchema } from "./schema/donation.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Donation", schema: DonationSchema }])
  ],
  providers: [DonationsService],
  controllers: [DonationsController],
  exports: [DonationsService]
})
export class DonationsModule {}
