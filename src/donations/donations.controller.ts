import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Query,
  Patch
} from "@nestjs/common";
import { DonationDto } from "./dto/donation.dto";
import { DonationsService } from "./donations.service";

@Controller("donations")
export class DonationsController {
  constructor(private readonly donationService: DonationsService) {}

  @Get()
  list(@Query() query): Promise<DonationDto> {
    return this.donationService.listActive(query.page, query.limit);
  }

  @Get(":id")
  find(@Param("id") id): Promise<DonationDto> {
    return this.donationService.find(id);
  }

  @Post()
  createDonation(@Body() donationDto: DonationDto): Promise<DonationDto> {
    return this.donationService.create(donationDto);
  }

  @Patch(":id")
  update(
    @Param("id") id,
    @Body() donationDto: DonationDto
  ): Promise<DonationDto> {
    return this.donationService.update(id, donationDto);
  }
}
