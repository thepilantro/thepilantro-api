import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateModel } from "mongoose-paginate-v2";
import { DonationDto } from "./dto/donation.dto";
import { ObjectID } from "mongodb";

@Injectable()
export class DonationsService {
  constructor(
    @InjectModel("Donation")
    private readonly donationModel: PaginateModel<DonationDto>
  ) {}

  listActive(page: number, limit: number): Promise<DonationDto> {
    const query = {};
    const options = {
      sort: { createdAt: -1 },
      limit: Number(limit),
      page: Number(page)
    };

    return this.donationModel.paginate(query, options);
  }

  find(id: ObjectID): Promise<DonationDto> {
    return this.donationModel.findById(id);
  }

  create(donationDto: DonationDto): Promise<DonationDto> {
    const createDonation = new this.donationModel(donationDto);
    return createDonation.save();
  }

  update(id: ObjectID, donationDto: DonationDto): Promise<DonationDto> {
    return this.donationModel.findByIdAndUpdate(id, donationDto);
  }
}
