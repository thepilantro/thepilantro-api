import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

export const DonationSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    ngoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Ngo"
    },
    amount: {
      type: String,
      default: ""
    },
    comment: {
      type: String
    },
    isAnonymous: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

DonationSchema.plugin(mongoosePaginate);
