import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Query,
  Patch
} from "@nestjs/common";
import { NgoService } from "./ngo.service";
import { NgoDto } from "./dto/ngo.dto";

@Controller("ngo")
export class NgoController {
  constructor(private readonly ngoService: NgoService) {}

  @Get()
  list(@Query() query): Promise<NgoDto> {
    return this.ngoService.listActive(query.page, query.limit);
  }

  @Get(":id")
  find(@Param("id") id): Promise<NgoDto> {
    return this.ngoService.find(id);
  }

  @Post()
  createNgo(@Body() ngoDto: NgoDto): Promise<NgoDto> {
    return this.ngoService.createNgo(ngoDto);
  }

  @Patch(":id")
  update(@Param("id") id, @Body() NgoDto: NgoDto): Promise<NgoDto> {
    return this.ngoService.update(id, NgoDto);
  }
}
