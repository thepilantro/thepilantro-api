import { Module } from "@nestjs/common";
import { NgoController } from "./ngo.controller";
import { NgoService } from "./ngo.service";
import { MongooseModule } from "@nestjs/mongoose";
import { NgoSchema } from "./schema/ngo.schema";

@Module({
  imports: [MongooseModule.forFeature([{ name: "Ngo", schema: NgoSchema }])],
  controllers: [NgoController],
  providers: [NgoService],
  exports: [NgoService]
})
export class NgoModule {}
