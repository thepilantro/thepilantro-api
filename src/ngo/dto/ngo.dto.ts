import { ObjectID } from "mongodb";

export class NgoDto {
  _id: ObjectID;
  name: string;
  description: string;
  category: string;
}
