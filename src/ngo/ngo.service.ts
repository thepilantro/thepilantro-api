import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { NgoDto } from "./dto/ngo.dto";
import { PaginateModel } from "mongoose-paginate-v2";
import { ObjectID } from "mongodb";

@Injectable()
export class NgoService {
  constructor(
    @InjectModel("Ngo")
    private readonly ngoModel: PaginateModel<NgoDto>
  ) {}

  listActive(page: number, limit: number): Promise<NgoDto> {
    const query = {};
    const options = {
      sort: { createdAt: -1 },
      limit: Number(limit),
      page: Number(page)
    };

    return this.ngoModel.paginate(query, options);
  }

  find(id: ObjectID): Promise<NgoDto> {
    return this.ngoModel.findById(id);
  }

  createNgo(ngoDto: NgoDto): Promise<NgoDto> {
    const creatNgo = new this.ngoModel(ngoDto);
    return creatNgo.save();
  }

  update(id: ObjectID, userDto: NgoDto): Promise<NgoDto> {
    console.log(userDto);
    return this.ngoModel.findByIdAndUpdate(id, userDto);
  }
}
