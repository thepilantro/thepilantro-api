import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

export const NgoSchema = new mongoose.Schema(
  {
    name: {
      type: String
    },
    description: {
      type: String
    },
    category: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

NgoSchema.plugin(mongoosePaginate);
