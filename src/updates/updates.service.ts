import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateModel } from "mongoose-paginate-v2";
import { UpdateDto } from "./dto/update.dto";

@Injectable()
export class UpdatesService {
  constructor(
    @InjectModel("Update")
    private readonly donationModel: PaginateModel<UpdateDto>
  ) {}

  createUpdate(updateDto: UpdateDto): Promise<UpdateDto> {
    const createUpdate = new this.donationModel(updateDto);
    return createUpdate.save();
  }
}
