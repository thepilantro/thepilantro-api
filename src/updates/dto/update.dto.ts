import { ObjectID } from "mongodb";

export class UpdateDto {
  _id: ObjectID;
  ngoId: ObjectID;
  content: string;
}
