import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

export const UpdateSchema = new mongoose.Schema(
  {
    ngoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Ngo"
    },
    content: {
      type: String,
      default: ""
    }
  },
  {
    timestamps: true
  }
);

UpdateSchema.plugin(mongoosePaginate);
