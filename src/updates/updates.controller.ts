import { Controller, Post, Body } from "@nestjs/common";
import { UpdatesService } from "./updates.service";
import { UpdateDto } from "./dto/update.dto";

@Controller("updates")
export class UpdatesController {
  constructor(private readonly updateService: UpdatesService) {}

  @Post()
  createUpdate(@Body() updateDto: UpdateDto): Promise<UpdateDto> {
    return this.updateService.createUpdate(updateDto);
  }
}
