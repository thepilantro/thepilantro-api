import { Module } from "@nestjs/common";
import { UpdatesController } from "./updates.controller";
import { UpdatesService } from "./updates.service";
import { MongooseModule } from "@nestjs/mongoose";
import { UpdateSchema } from "./schema/update.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Update", schema: UpdateSchema }])
  ],
  controllers: [UpdatesController],
  providers: [UpdatesService]
})
export class UpdatesModule {}
