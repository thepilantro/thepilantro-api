import { Module } from "@nestjs/common";
import { SubscriptionsController } from "./subscriptions.controller";
import { SubscriptionsService } from "./subscriptions.service";
import { MongooseModule } from "@nestjs/mongoose";
import { SubscriptionSchema } from "./schema/subscription.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Subscription", schema: SubscriptionSchema }
    ])
  ],
  controllers: [SubscriptionsController],
  providers: [SubscriptionsService]
})
export class SubscriptionsModule {}
