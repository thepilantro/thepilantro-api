import { Controller, Post, Body } from "@nestjs/common";
import { SubscriptionsService } from "./subscriptions.service";
import { SubscriptionDto } from "./dto/subscription.dto";

@Controller("subscriptions")
export class SubscriptionsController {
  constructor(private readonly subscriptionService: SubscriptionsService) {}

  @Post()
  createSubscription(
    @Body() subscriptionDto: SubscriptionDto
  ): Promise<SubscriptionDto> {
    return this.subscriptionService.createSubscription(subscriptionDto);
  }
}
