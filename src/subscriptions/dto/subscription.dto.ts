import { ObjectID } from "mongodb";

export class SubscriptionDto {
  _id: ObjectID;
  ngoId: ObjectID;
  userID: ObjectID;
}
