import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateModel } from "mongoose-paginate-v2";
import { SubscriptionDto } from "./dto/subscription.dto";

@Injectable()
export class SubscriptionsService {
  constructor(
    @InjectModel("Subscription")
    private readonly subscriptionModel: PaginateModel<SubscriptionDto>
  ) {}

  createSubscription(
    subscriptionDto: SubscriptionDto
  ): Promise<SubscriptionDto> {
    const createSubscription = new this.subscriptionModel(subscriptionDto);
    return createSubscription.save();
  }
}
