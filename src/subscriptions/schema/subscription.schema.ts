import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

export const SubscriptionSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    ngoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Ngo"
    }
  },
  {
    timestamps: true
  }
);

SubscriptionSchema.plugin(mongoosePaginate);
