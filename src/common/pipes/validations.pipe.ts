import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  BadRequestException
} from "@nestjs/common";
import { validate } from "class-validator";
import { plainToClass } from "class-transformer";

@Injectable()
export class ValidationsPipe implements PipeTransform {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    // check value
    const object = plainToClass(metatype, value);

    // validate object
    const errors = await validate(object);

    if (errors.length > 0) {
      // throws the first error message only
      throw new BadRequestException(errors[0].constraints);
    }
    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}
