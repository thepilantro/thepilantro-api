import { ConfigService } from "./config.service";
const configService = new ConfigService(`.env`);

export default () => ({
  facebook: {
    username: configService.get("APP_FB_USERNAME"),
    password: configService.get("APP_FB_PASSWORD"),
    id: configService.get("APP_FB_ID"),
    secret: configService.get("APP_FB_SECRET"),
    clientToken: configService.get("APP_FB_CLIENT_TOKEN"),
    grantType: configService.get("APP_FB_GRANT_TYPE")
  },
  database: {
    uri: configService.get("APP_DATABASE_URI")
  }
});
