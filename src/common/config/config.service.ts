import * as dotenv from "dotenv";
import * as fs from "fs";

/**
 * @description Configuration service
 */
export class ConfigService {
  private readonly envConfig: { [key: string]: string };

  /**
   * Creates an instance of config service.
   * @param filePath
   */
  constructor(filePath: string) {
    console.log(typeof filePath);
    this.envConfig = dotenv.parse(fs.readFileSync(filePath));
  }

  /**
   * @description Gets value of environment file
   * @param key
   * @returns key name value
   */
  get(key: string): string {
    return this.envConfig[key];
  }
}
