import { ObjectID } from "mongodb";

export class UserDto {
  _id: ObjectID;
  imageUrl: string;
  firstname: string;
  lastname: string;
}
