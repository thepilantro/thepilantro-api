import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Query,
  Patch
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { UserDto } from "./dto/user.dto";

@Controller("users")
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  list(@Query() query): Promise<UserDto> {
    return this.userService.listActive(query.page, query.limit);
  }

  @Get(":id")
  find(@Param("id") id): Promise<UserDto> {
    return this.userService.find(id);
  }

  @Post()
  createUser(@Body() userDto: UserDto): Promise<UserDto> {
    return this.userService.create(userDto);
  }

  @Patch(":id")
  update(@Param("id") id, @Body() userDto: UserDto): Promise<UserDto> {
    return this.userService.update(id, userDto);
  }
}
