import { Injectable } from "@nestjs/common";
import { UserDto } from "./dto/user.dto";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateModel } from "mongoose-paginate-v2";
import { ObjectID } from "mongodb";

@Injectable()
export class UsersService {
  constructor(
    @InjectModel("User")
    private readonly userModel: PaginateModel<UserDto>
  ) {}

  listActive(page: number, limit: number): Promise<UserDto> {
    const query = {};
    const options = {
      sort: { createdAt: -1 },
      limit: Number(limit),
      page: Number(page)
    };

    return this.userModel.paginate(query, options);
  }

  find(id: ObjectID): Promise<UserDto> {
    return this.userModel.findById(id);
  }

  create(userDto: UserDto): Promise<UserDto> {
    const createUser = new this.userModel(userDto);
    return createUser.save();
  }

  update(id: ObjectID, userDto: UserDto): Promise<UserDto> {
    console.log(userDto);
    return this.userModel.findByIdAndUpdate(id, userDto);
  }
}
