import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

export const UserSchema = new mongoose.Schema(
  {
    imageUrl: {
      type: String,
      default: ""
    },
    firstname: {
      type: String
    },
    lastname: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

UserSchema.plugin(mongoosePaginate);
