import { Module, HttpModule } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { FacebookService } from "./facebook/facebook.service";
import { FacebookModule } from "./facebook/facebook.module";
import { UsersModule } from "./users/users.module";
import { AuthModule } from "./auth/auth.module";
import { DonationsModule } from './donations/donations.module';
import { NgoModule } from './ngo/ngo.module';
import { UpdatesModule } from './updates/updates.module';
import { SubscriptionsModule } from './subscriptions/subscriptions.module';
import configuration from "./common/config/configuration";

@Module({
  imports: [
    MongooseModule.forRoot(configuration().database.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    }),
    FacebookModule,
    UsersModule,
    AuthModule,
    HttpModule,
    DonationsModule,
    NgoModule,
    UpdatesModule,
    SubscriptionsModule
  ],
  controllers: [AppController],
  providers: [AppService, FacebookService]
})
export class AppModule {}
